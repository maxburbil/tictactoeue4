// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Data/Grid1dArray.h"
#include "Data/PlayerMarker.h"
#include "GameFramework/GameModeBase.h"
#include "TicTacToeGameMode.generated.h"

class ATicTacToeAIController;
class UBoard;
/**
 * 
 */
UCLASS()
class TICTACTOE_API ATicTacToeGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:

	bool TryToCaptureTile(TEnumAsByte<ETileState> PlayerMarker, int Row, int Column) const;
	void InitGrid(int NewGridSize, int NewMatchLineSize);
	
	bool IsRowCrossed(int Row, int Column, TEnumAsByte<ETileState> &Winner) const;
	bool IsColumnCrossed(int Row, int Column, TEnumAsByte<ETileState> &Winner) const;
	bool IsFirstDiagonalCrossed(int Row, int Column, TEnumAsByte<ETileState> &Winner) const;
	bool IsSecondDiagonalCrossed(int Row, int Column, TEnumAsByte<ETileState> &Winner) const;
	bool HasEmptyTile() const;
	int GetGridSize() const;
	
	void AITurn();
	void FirstAITurn();
	
	TArray<FGrid1dArray> GetGrid() const;
	
protected:

	virtual void PostLogin(APlayerController* NewPlayer) override;

	bool IsTileFree(int Row, int Column) const;

	ATicTacToeAIController* CreateAI();

private:
	
	UPROPERTY(BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	UBoard* Board;

	UPROPERTY()
	int GridSize;

	UPROPERTY()
	int MatchLineSize;

	UPROPERTY()
	ATicTacToeAIController* AIController;
};
