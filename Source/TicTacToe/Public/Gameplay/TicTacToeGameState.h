// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Data/Grid1dArray.h"
#include "Data/PlayerMarker.h"
#include "GameFramework/GameStateBase.h"
#include "TicTacToeGameState.generated.h"

class ATicTacToePlayerState;
class ATicTacToeGameMode;
class UBoard;
/**
 * 
 */
UCLASS()
class TICTACTOE_API ATicTacToeGameState : public AGameStateBase
{
	GENERATED_BODY()

public:

	void StartGame();
	
	void SetupFirstPlayer(ATicTacToePlayerState* PlayerState);
	void SetupSecondPlayer(ATicTacToePlayerState* PlayerState);
	
	void UpdateViewForPlayers(TEnumAsByte<ETileState> PlayerMarker, int Row, int Column);
	void UpdateVictoryLine(ETileState Winner);
	void UpdateDrawLine();
	bool TryToCaptureTile(TEnumAsByte<ETileState> PlayerMarker, int Row, int Column);
	void Init(int GridSize);
	
	TEnumAsByte<ETileState> GetCurrentActivePlayer() const;

	UPROPERTY(BlueprintReadOnly)
	TEnumAsByte<ETileState> FirstPlayerMarker;

	UPROPERTY(BlueprintReadOnly)
	TEnumAsByte<ETileState> SecondPlayerMarker;

	UPROPERTY()
	ATicTacToePlayerState* FirstPlayerState;

	UPROPERTY()
	ATicTacToePlayerState* SecondPlayerState;

	bool CheckVictory(int Row, int Column, TEnumAsByte<ETileState> &Winner) const;
	bool CheckDraw() const;

	UPROPERTY()
	int DrawCounter;

	UPROPERTY()
	int DrawMaxCounter;
	
protected:
	
	virtual void BeginPlay() override;

	bool CheckIsDraw() const;
	void TogglePlayer();
	
	UPROPERTY(BlueprintReadOnly)
	TEnumAsByte<ETileState> CurrentActivePlayer = X_STATE;

private:
	
	UPROPERTY()
	ATicTacToeGameMode* GameMode;

};


