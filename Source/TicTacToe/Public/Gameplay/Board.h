// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Data/Grid1dArray.h"
#include "UObject/NoExportTypes.h"
#include "Board.generated.h"

/**
 * 
 */
UCLASS()
class TICTACTOE_API UBoard : public UObject
{
	GENERATED_BODY()

public:
	 void Init(int SizeOfGrid);
	
	UPROPERTY(BlueprintReadOnly)
	TArray<FGrid1dArray> Grid;
private:

	

	UPROPERTY()
	int Size;
};
