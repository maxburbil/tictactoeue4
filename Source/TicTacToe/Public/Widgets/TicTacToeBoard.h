// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "TicTacToeBoard.generated.h"

class UUniformGridPanel;
class UBoardTile;

UCLASS(Abstract)
class TICTACTOE_API UTicTacToeBoard : public UUserWidget
{
	GENERATED_BODY()

public:

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (BindWidget))
	UUniformGridPanel* BoardGrid;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Settings")
	TSubclassOf<UBoardTile> BoardTileSubclass;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Settings|Test")
	int GridSize;

	void UpdateTile(UTexture2D* Texture, int Row, int Column);
	
	void GenerateGrid(int NewGridSize);
};
