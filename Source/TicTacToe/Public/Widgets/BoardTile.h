// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "BoardTile.generated.h"

class UButton;
class UImage;

UCLASS(Abstract)
class TICTACTOE_API UBoardTile : public UUserWidget
{
	GENERATED_BODY()

public:

	virtual bool Initialize() override;
	void UpdateView(UTexture2D* Texture) const;
	
protected:
	
	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, meta = (BindWidget))
	UButton* TileButton;

	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, meta = (BindWidget))
	UImage* MarkerImage;

private:

	UFUNCTION()
	void OnTileButtonClicked();
};
