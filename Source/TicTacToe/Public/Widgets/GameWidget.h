// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Data/Grid1dArray.h"
#include "Data/PlayerMarker.h"
#include "GameWidget.generated.h"

class UTicTacToeBoard;
class USlider;
class UButton;
class UTextBlock;

UCLASS(Abstract)
class TICTACTOE_API UGameWidget : public UUserWidget
{
	GENERATED_BODY()

public:

	virtual bool Initialize() override;
	
	void UpdateTile(UTexture2D* Texture, int Row, int Column) const;

	void UpdateWinner(TEnumAsByte<ETileState> Winner);
	void UpdateDraw();

protected:
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (BindWidget))
	UTicTacToeBoard* BoardWidget;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (BindWidget))
	USlider* GridSizeSlider;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (BindWidget))
	USlider* MatchLineSizeSlider;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (BindWidget))
	UButton* StartButton;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (BindWidget))
	UButton* ExitButton;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (BindWidget))
	UTextBlock* WinnerLine;

private:
	
	UFUNCTION()
	void OnStartButton();
	
	UFUNCTION()
	void OnExitButton();
};