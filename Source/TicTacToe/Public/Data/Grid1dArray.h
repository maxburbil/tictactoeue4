// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Grid1dArray.generated.h"

UENUM()
enum ETileState
{
	FREE,
	X_STATE,
	O_STATE
};

USTRUCT(BlueprintType)
struct FGrid1dArray
{
	GENERATED_BODY()
	
	TEnumAsByte<ETileState>& operator[] (const int32 TileIndex)
	{
		return InnerArray[TileIndex];
	}

	const TEnumAsByte<ETileState>& operator[](const int32 TileIndex) const
	{
		return InnerArray[TileIndex];
	}

	void Add(TEnumAsByte<ETileState> NewElem)
	{
		InnerArray.Add(NewElem);
	}

private:
	
	UPROPERTY(BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	TArray<TEnumAsByte<ETileState>> InnerArray;
};