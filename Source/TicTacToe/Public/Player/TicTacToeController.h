// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Data/Grid1dArray.h"
#include "Data/PlayerMarker.h"
#include "GameFramework/PlayerController.h"
#include "TicTacToeController.generated.h"

class UGameWidget;

UCLASS()
class TICTACTOE_API ATicTacToeController : public APlayerController
{
	GENERATED_BODY()

public:
	ATicTacToeController();

	void TryToCaptureTile(TEnumAsByte<ETileState> PlayerMarker, int Row, int Column);
	void UpdateTileView(TEnumAsByte<ETileState> PlayerMarker, int Row, int Column);
	void UpdateWinnerLine(TEnumAsByte<ETileState> Winner);
	void UpdateDrawLine();
	
protected:

	virtual void BeginPlay() override;

private:
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	TMap<TEnumAsByte<ETileState>, UTexture2D*> MarkerTextures;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	TSubclassOf<UGameWidget> GameUISubclass;
	
	UPROPERTY(BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	UGameWidget* GameUI;
};
