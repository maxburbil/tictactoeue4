// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Data/Grid1dArray.h"
#include "Data/PlayerMarker.h"
#include "GameFramework/PlayerState.h"
#include "TicTacToePlayerState.generated.h"

/**
 * 
 */
UCLASS()
class TICTACTOE_API ATicTacToePlayerState : public APlayerState
{
	GENERATED_BODY()
	
public:

	void SetPlayerMarker(TEnumAsByte<ETileState> NewPlayerMarker);
	TEnumAsByte<ETileState> GetPlayerMarker() const;
	
protected:
	
	UPROPERTY(BlueprintReadOnly)
	TEnumAsByte<ETileState> PlayerMarker;
};
