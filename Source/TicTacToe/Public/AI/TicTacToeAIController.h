// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "Data/Grid1dArray.h"
#include "TicTacToeAIController.generated.h"

class ATicTacToeGameState;
class ATicTacToeGameMode;
/**
 * 
 */
UCLASS()
class TICTACTOE_API ATicTacToeAIController : public AAIController
{
	GENERATED_BODY()

public:

	ATicTacToeAIController();
	void BestMove(TArray<FGrid1dArray>& Grid);
	void FirstAIRandomMove(int Row, int Column);
	
protected:
	
	virtual void BeginPlay() override;
	
	TArray<TPair<int, int>> GetLegalMoves() const;
	TPair<int, TPair<int, int>> Minimax(TArray<FGrid1dArray>& Grid, int Depth, bool bIsMaximizing, int Alpha, int Beta, int Row, int Column);

private:

	UPROPERTY()
	ATicTacToeGameMode* GameMode;

	UPROPERTY()
	ATicTacToeGameState* GameState;
	
};
