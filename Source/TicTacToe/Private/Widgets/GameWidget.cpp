// Fill out your copyright notice in the Description page of Project Settings.

#include "Widgets/GameWidget.h"
#include "Components/Button.h"
#include "Components/Slider.h"
#include "Components/TextBlock.h"
#include "Gameplay/TicTacToeGameMode.h"
#include "Gameplay/TicTacToeGameState.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Widgets/TicTacToeBoard.h"

bool UGameWidget::Initialize()
{
	bInitialized =  Super::Initialize();
	if (bInitialized)
	{
		BoardWidget->SetVisibility(ESlateVisibility::Collapsed);
		StartButton->OnClicked.AddDynamic(this, &UGameWidget::OnStartButton);
		ExitButton->OnClicked.AddDynamic(this, &UGameWidget::OnExitButton);
		return true;
	}
	return false;
}

void UGameWidget::UpdateTile(UTexture2D* Texture, int Row, int Column) const
{
	BoardWidget->UpdateTile(Texture, Row, Column);
}

void UGameWidget::UpdateWinner(TEnumAsByte<ETileState> Winner)
{
	BoardWidget->SetIsEnabled(false);
	FString WinnerText;
	if (Winner == X_STATE)
	{
		WinnerText = "X wins the game";
	}
	else
	{
		WinnerText = "O wins the game";
	}
	WinnerLine->SetText( FText::FromString(WinnerText));
	WinnerLine->SetVisibility(ESlateVisibility::Visible);
}

void UGameWidget::UpdateDraw()
{
	BoardWidget->SetIsEnabled(false);
	WinnerLine->SetText(FText::FromString("It`s draw. Try again"));
	WinnerLine->SetVisibility(ESlateVisibility::Visible);
}

void UGameWidget::OnStartButton()
{
	ATicTacToeGameMode* GameMode = GetWorld()->GetAuthGameMode<ATicTacToeGameMode>();
	GameMode->InitGrid(GridSizeSlider->GetValue(), MatchLineSizeSlider->GetValue());
	
	BoardWidget->GenerateGrid(GridSizeSlider->GetValue());
	BoardWidget->SetIsEnabled(true);
	WinnerLine->SetVisibility(ESlateVisibility::Hidden);
	ATicTacToeGameState* GameState = GetWorld()->GetGameState<ATicTacToeGameState>();
	GameState->StartGame();
}

void UGameWidget::OnExitButton()
{
	UKismetSystemLibrary::QuitGame(this, GetOwningPlayer(), EQuitPreference::Quit, false);
}
