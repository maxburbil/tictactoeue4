// Fill out your copyright notice in the Description page of Project Settings.


#include "Widgets/TicTacToeBoard.h"
#include "Components/UniformGridPanel.h"
#include "Components/UniformGridSlot.h"
#include "Widgets/BoardTile.h"

void UTicTacToeBoard::UpdateTile(UTexture2D* Texture, int Row, int Column)
{
	const UBoardTile* Tile = Cast<UBoardTile>(BoardGrid->GetChildAt(Row * GridSize + Column));
	Tile->UpdateView(Texture);
}

void UTicTacToeBoard::GenerateGrid(int NewGridSize)
{
	SetVisibility(ESlateVisibility::SelfHitTestInvisible);
	BoardGrid->ClearChildren();
	GridSize = NewGridSize;
	for (int Row = 0; Row < GridSize; ++Row)
	{
		for (int Column = 0; Column < GridSize; ++Column)
		{
			UBoardTile* Tile = CreateWidget<UBoardTile>(GetWorld(), BoardTileSubclass);
			UUniformGridSlot* GridSlot = BoardGrid->AddChildToUniformGrid(Tile, Row, Column);
			
			GridSlot->SetHorizontalAlignment(HAlign_Fill);
			GridSlot->SetVerticalAlignment(VAlign_Fill);
		}
	}
}