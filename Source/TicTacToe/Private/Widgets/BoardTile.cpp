// Fill out your copyright notice in the Description page of Project Settings.


#include "Widgets/BoardTile.h"
#include "Components/Button.h"
#include "Components/Image.h"
#include "Components/PanelWidget.h"
#include "Components/UniformGridSlot.h"
#include "Gameplay/TicTacToeGameState.h"
#include "Player/TicTacToeController.h"
#include "Player/TicTacToePlayerState.h"

bool UBoardTile::Initialize()
{
	bInitialized = Super::Initialize();

	if (bInitialized)
	{
		TileButton->OnClicked.AddDynamic(this, &UBoardTile::OnTileButtonClicked);
		return true;
	}
	return false;
}

void UBoardTile::UpdateView(UTexture2D* Texture) const
{
	TileButton->SetIsEnabled(false);
	MarkerImage->SetBrushFromTexture(Texture);
}

void UBoardTile::OnTileButtonClicked()
{
	const UUniformGridSlot* GridSlot = Cast<UUniformGridSlot>(Slot);
	ATicTacToeController* PlayerController = Cast<ATicTacToeController>(GetOwningPlayer());
	const ATicTacToePlayerState* PlayerState = PlayerController->GetPlayerState<ATicTacToePlayerState>();
	const ATicTacToeGameState* GameState = GetWorld()->GetGameState<ATicTacToeGameState>();

	if (GameState->GetCurrentActivePlayer() == PlayerState->GetPlayerMarker())
	{
		PlayerController->TryToCaptureTile(PlayerState->GetPlayerMarker(), GridSlot->Row, GridSlot->Column);
	}
}