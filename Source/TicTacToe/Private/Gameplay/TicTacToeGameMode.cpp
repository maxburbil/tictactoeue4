// Fill out your copyright notice in the Description page of Project Settings.


#include "Gameplay/TicTacToeGameMode.h"
#include "AI/TicTacToeAIController.h"
#include "Gameplay/Board.h"
#include "Gameplay/TicTacToeGameState.h"
#include "Player/TicTacToePlayerState.h"

bool ATicTacToeGameMode::TryToCaptureTile(TEnumAsByte<ETileState> PlayerMarker, int Row, int Column) const
{
	if (IsTileFree(Row, Column))
	{
		Board->Grid[Row][Column] = PlayerMarker;
		return true;
	}
	return false;
}

void ATicTacToeGameMode::InitGrid(int NewGridSize, int NewMatchLineSize)
{
	GridSize = NewGridSize;
	MatchLineSize = NewMatchLineSize;
	
	Board = NewObject<UBoard>();	
	Board->Init(GridSize);
	
	ATicTacToeGameState* TicTacToeGameState = GetGameState<ATicTacToeGameState>();
	TicTacToeGameState->Init(GridSize);
}

bool ATicTacToeGameMode::IsTileFree(int Row, int Column) const
{
	return Board->Grid[Row][Column] == FREE;
}

ATicTacToeAIController* ATicTacToeGameMode::CreateAI()
{
	FActorSpawnParameters SpawnInfo;
	SpawnInfo.Instigator = nullptr;
	SpawnInfo.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	SpawnInfo.OverrideLevel = nullptr;

	UWorld* World = GetWorld();
	ATicTacToeAIController* AIC = World->SpawnActor<ATicTacToeAIController>(SpawnInfo);
	
	return AIC;
}

bool ATicTacToeGameMode::IsRowCrossed(int Row, int Column, TEnumAsByte<ETileState> &Winner) const
{
	if (Board->Grid[Row][Column] == FREE)
	{
		return false;
	}
	int MaxSubsequence = 1;
	for (int i = Column - 1; i >= 0; --i)
	{
		if (Board->Grid[Row][i] == Board->Grid[Row][Column])
		{
			if (++MaxSubsequence >= MatchLineSize)
			{
				Winner = Board->Grid[Row][Column];
				return true;
			}
			continue;
		}
		break;
	}
	
	for (int i = Column + 1; i < GridSize; ++i)
	{
		if (Board->Grid[Row][i] == Board->Grid[Row][Column])
		{
			if (++MaxSubsequence >= MatchLineSize)
			{
				Winner = Board->Grid[Row][Column];
				return true;
			}
			continue;
		}
		break;
	}
	return false;
}

bool ATicTacToeGameMode::IsColumnCrossed(int Row, int Column, TEnumAsByte<ETileState> &Winner) const
{
	if (Board->Grid[Row][Column] == FREE)
	{
		return false;
	}
	int MaxSubsequence = 1;
	for (int i = Row - 1; i >= 0; --i)
	{
		if (Board->Grid[i][Column] == Board->Grid[Row][Column])
		{
			if (++MaxSubsequence >= MatchLineSize)
			{
				Winner = Board->Grid[Row][Column];
				return true;
			}
			continue;
		}
		break;
	}
	
	for (int i = Row + 1; i < GridSize; ++i)
	{
		if (Board->Grid[i][Column] == Board->Grid[Row][Column])
		{
			if (++MaxSubsequence >= MatchLineSize)
			{
				Winner = Board->Grid[Row][Column];
				return true;
			}
			continue;
		}
		break;
	}
	return false; 
}

bool ATicTacToeGameMode::IsFirstDiagonalCrossed(int Row, int Column, TEnumAsByte<ETileState> &Winner) const
{
	if (Board->Grid[Row][Column] == FREE)
	{
		return false;
	}
	int MaxSubsequence = 1;
	int RowCounter = Row - 1, ColumnCounter = Column - 1;
	while (RowCounter >= 0 && ColumnCounter >= 0)
	{
		if (Board->Grid[RowCounter][ColumnCounter] == Board->Grid[Row][Column])
		{
			if (++MaxSubsequence >= MatchLineSize)
			{
				Winner = Board->Grid[Row][Column];
				return true;
			}
			--RowCounter;
			--ColumnCounter;
			continue;
		}
		break;
	}
	RowCounter = Row + 1, ColumnCounter = Column + 1;
	
	while (RowCounter < GridSize && ColumnCounter < GridSize)
	{
		if (Board->Grid[RowCounter][ColumnCounter] == Board->Grid[Row][Column])
		{
			if (++MaxSubsequence >= MatchLineSize)
			{
				Winner = Board->Grid[Row][Column];
				return true;
			}
			++RowCounter;
			++ColumnCounter;
			continue;
		}
		break;
	}
	return false;
}

bool ATicTacToeGameMode::IsSecondDiagonalCrossed(int Row, int Column, TEnumAsByte<ETileState> &Winner) const
{
	if (Board->Grid[Row][Column] == FREE)
	{
		return false;
	}
	int MaxSubsequence = 1;
	int RowCounter = Row - 1, ColumnCounter = Column + 1;
	while (RowCounter >= 0 && ColumnCounter < GridSize)
	{
		if (Board->Grid[RowCounter][ColumnCounter] == Board->Grid[Row][Column])
		{
			if (++MaxSubsequence >= MatchLineSize)
			{
				Winner = Board->Grid[Row][Column];
				return true;
			}
			--RowCounter;
			++ColumnCounter;
			continue;
		}
		break;
	}
	RowCounter = Row + 1, ColumnCounter = Column - 1;
	
	while (RowCounter < GridSize && ColumnCounter >= 0)
	{
		if (Board->Grid[RowCounter][ColumnCounter] == Board->Grid[Row][Column])
		{
			if (++MaxSubsequence >= MatchLineSize)
			{
				Winner = Board->Grid[Row][Column];
				return true;
			}
			++RowCounter;
			--ColumnCounter;
			continue;
		}
		break;
	}
	return false;
	
}

bool ATicTacToeGameMode::HasEmptyTile() const
{
	for (int Row = 0; Row < GridSize; ++Row)
	{
		for (int Column = 0; Column < GridSize; ++Column)
		{
			if (Board->Grid[Row][Column] == FREE)
			{
				return true;
			}
		}
	}
	return false;
}

int ATicTacToeGameMode::GetGridSize() const
{
	return GridSize;
}

void ATicTacToeGameMode::AITurn()
{
	AIController->BestMove(Board->Grid);
}

void ATicTacToeGameMode::FirstAITurn()
{
	const int Row = FMath::RandRange(0, GridSize - 1);
	const int Column = FMath::RandRange(0, GridSize - 1);
	AIController->FirstAIRandomMove(Row, Column);
}

TArray<FGrid1dArray> ATicTacToeGameMode::GetGrid() const
{
	return Board->Grid;
}

void ATicTacToeGameMode::PostLogin(APlayerController* NewPlayer)
{
	Super::PostLogin(NewPlayer);
	ATicTacToeGameState* TicTacToeGameState = GetGameState<ATicTacToeGameState>();
	TicTacToeGameState->SetupFirstPlayer(NewPlayer->GetPlayerState<ATicTacToePlayerState>());

	AIController = CreateAI();
	TicTacToeGameState->SetupSecondPlayer(AIController->GetPlayerState<ATicTacToePlayerState>());
}
