// Fill out your copyright notice in the Description page of Project Settings.


#include "Gameplay/Board.h"

void UBoard::Init(int SizeOfGrid)
{
	Size = SizeOfGrid;

	for	(int Row = 0; Row < Size; ++Row)
	{
		Grid.Add(FGrid1dArray());
		for (int Column = 0; Column < Size; ++Column)
		{
			Grid[Row].Add(FREE);
		}
	}
}
