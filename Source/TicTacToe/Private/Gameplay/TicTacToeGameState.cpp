// Fill out your copyright notice in the Description page of Project Settings.

#include "Gameplay/TicTacToeGameState.h"
#include "AI/TicTacToeAIController.h"
#include "GameFramework/PlayerState.h"
#include "Gameplay/TicTacToeGameMode.h"
#include "Player/TicTacToeController.h"
#include "Player/TicTacToePlayerState.h"

void ATicTacToeGameState::StartGame()
{
	if (CurrentActivePlayer == SecondPlayerMarker)
	{
		GameMode->FirstAITurn();
	}
}

void ATicTacToeGameState::SetupFirstPlayer(ATicTacToePlayerState* PlayerState)
{
	PlayerState->SetPlayerMarker(FirstPlayerMarker);
	FirstPlayerState = PlayerState;
}

void ATicTacToeGameState::SetupSecondPlayer(ATicTacToePlayerState* PlayerState)
{
	PlayerState->SetPlayerMarker(SecondPlayerMarker);
	SecondPlayerState = PlayerState;
}

void ATicTacToeGameState::UpdateViewForPlayers(TEnumAsByte<ETileState> PlayerMarker, int Row, int Column)
{
	for (const APlayerState* PlayerState: PlayerArray)
	{
		ATicTacToeController* Controller = Cast<ATicTacToeController>(PlayerState->GetOwner());
		if (Controller)
		{
			Controller->UpdateTileView(PlayerMarker, Row, Column);
		}
	}
}

void ATicTacToeGameState::UpdateVictoryLine(ETileState Winner)
{
	for (const APlayerState* PlayerState: PlayerArray)
	{
		ATicTacToeController* Controller = Cast<ATicTacToeController>(PlayerState->GetOwner());
		if (Controller)
		{
			Controller->UpdateWinnerLine(Winner);
		}
	}
}

void ATicTacToeGameState::UpdateDrawLine()
{
	for (const APlayerState* PlayerState: PlayerArray)
	{
		ATicTacToeController* Controller = Cast<ATicTacToeController>(PlayerState->GetOwner());
		if (Controller)
		{
			Controller->UpdateDrawLine();
		}
	}
}

bool ATicTacToeGameState::TryToCaptureTile(TEnumAsByte<ETileState> PlayerMarker, int Row, int Column)
{
	const bool bIsSuccess = GameMode->TryToCaptureTile(PlayerMarker, Row, Column);
	if (bIsSuccess)
	{
		++DrawCounter;
		UpdateViewForPlayers(PlayerMarker, Row, Column);
		if (CheckVictory(Row, Column, PlayerMarker))
		{
			UpdateVictoryLine(PlayerMarker);
			return true;
		}
		if (CheckIsDraw())
		{
			UpdateDrawLine();
			return true;
		}

		TogglePlayer();
		return true;
	}
	return false;
}

void ATicTacToeGameState::Init(int GridSize)
{
	DrawCounter = 0;
	DrawMaxCounter = GridSize * GridSize;
	CurrentActivePlayer = X_STATE;
	
	const int FirstPlayerMarkerRes = FMath::RandRange(1, 2);
	if (FirstPlayerMarkerRes == 1)
	{
		FirstPlayerMarker = X_STATE;
		SecondPlayerMarker = O_STATE; 
	}
	else
	{
		FirstPlayerMarker = O_STATE;
		SecondPlayerMarker = X_STATE;
	}
	FirstPlayerState->SetPlayerMarker(FirstPlayerMarker);
	SecondPlayerState->SetPlayerMarker(SecondPlayerMarker);
}

TEnumAsByte<ETileState> ATicTacToeGameState::GetCurrentActivePlayer() const
{
	return CurrentActivePlayer;
}

void ATicTacToeGameState::BeginPlay()
{
	Super::BeginPlay();
	
	GameMode = GetWorld()->GetAuthGameMode<ATicTacToeGameMode>();	
}

bool ATicTacToeGameState::CheckIsDraw() const
{
	return DrawCounter >= DrawMaxCounter;
}

void ATicTacToeGameState::TogglePlayer()
{
	if (CurrentActivePlayer == FirstPlayerMarker)
	{
		CurrentActivePlayer = SecondPlayerMarker;
		GameMode->AITurn();
		return;
	}
	CurrentActivePlayer = FirstPlayerMarker;
}

bool ATicTacToeGameState::CheckVictory(int Row, int Column, TEnumAsByte<ETileState> &Winner) const
{
	return GameMode->IsRowCrossed(Row, Column, Winner) || GameMode->IsColumnCrossed(Row, Column, Winner) ||
		GameMode->IsFirstDiagonalCrossed(Row, Column, Winner) || GameMode->IsSecondDiagonalCrossed(Row, Column, Winner);
}

bool ATicTacToeGameState::CheckDraw() const
{
	return GameMode->HasEmptyTile();
}
