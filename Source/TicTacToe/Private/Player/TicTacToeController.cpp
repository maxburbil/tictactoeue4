// Fill out your copyright notice in the Description page of Project Settings.

#include "Player/TicTacToeController.h"
#include "Blueprint/UserWidget.h"
#include "Gameplay/TicTacToeGameState.h"
#include "Widgets/GameWidget.h"

ATicTacToeController::ATicTacToeController()
{
	SetShowMouseCursor(true);
}

void ATicTacToeController::TryToCaptureTile(TEnumAsByte<ETileState> PlayerMarker, int Row, int Column)
{
	ATicTacToeGameState* GameState = GetWorld()->GetGameState<ATicTacToeGameState>();
	GameState->TryToCaptureTile(PlayerMarker, Row, Column);
}

void ATicTacToeController::UpdateTileView(TEnumAsByte<ETileState> PlayerMarker, int Row, int Column)
{
	UTexture2D* MarkerTexture = MarkerTextures[PlayerMarker];
	GameUI->UpdateTile(MarkerTexture, Row, Column);
}

void ATicTacToeController::UpdateWinnerLine(TEnumAsByte<ETileState> Winner)
{
	GameUI->UpdateWinner(Winner);
}

void ATicTacToeController::UpdateDrawLine()
{
	GameUI->UpdateDraw();
}

void ATicTacToeController::BeginPlay()
{
	Super::BeginPlay();

	if (GameUISubclass == nullptr)
	{
		UE_LOG(LogController, Fatal, TEXT("GameUISubclass is null. Can't create game widget"));
	}
	GameUI = CreateWidget<UGameWidget>(this, GameUISubclass);
	GameUI->AddToViewport();
}
