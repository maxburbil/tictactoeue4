// Fill out your copyright notice in the Description page of Project Settings.


#include "Player/TicTacToePlayerState.h"


void ATicTacToePlayerState::SetPlayerMarker(TEnumAsByte<ETileState> NewPlayerMarker)
{
	PlayerMarker = NewPlayerMarker;
}

TEnumAsByte<ETileState> ATicTacToePlayerState::GetPlayerMarker() const
{
	return PlayerMarker;
}
