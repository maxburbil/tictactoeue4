// Fill out your copyright notice in the Description page of Project Settings.


#include "AI/TicTacToeAIController.h"
#include "Gameplay/TicTacToeGameMode.h"
#include "Gameplay/TicTacToeGameState.h"
#include "Player/TicTacToePlayerState.h"

ATicTacToeAIController::ATicTacToeAIController()
{
	bWantsPlayerState = 1;
}

void ATicTacToeAIController::BeginPlay()
{
	Super::BeginPlay();

	GameMode = GetWorld()->GetAuthGameMode<ATicTacToeGameMode>();
	GameState = GetWorld()->GetGameState<ATicTacToeGameState>();
}

TArray<TPair<int, int>> ATicTacToeAIController::GetLegalMoves() const
{
	TArray<TPair<int, int>> Result;
	const TArray<FGrid1dArray> Grid = GameMode->GetGrid(); 
	const int GridSize = Grid.Num();
	for (int Row = 0; Row < GridSize; ++Row)
	{
		for (int Column = 0; Column < GridSize; ++Column)
		{
			if (Grid[Row][Column] == FREE)
			{
				Result.Add(TPair<int, int>(Row, Column));
			}	
		}
	}
	return Result;
}

void ATicTacToeAIController::BestMove(TArray<FGrid1dArray>& Grid)
{
	const ATicTacToePlayerState* TicTacToePlayerState = GetPlayerState<ATicTacToePlayerState>();
	
	TPair<int, TPair<int, int>> BestMove = Minimax(Grid, 3, true, INT_MIN, INT_MAX, 0, 0);
	GameState->TryToCaptureTile(TicTacToePlayerState->GetPlayerMarker(), BestMove.Value.Key, BestMove.Value.Value);
}

void ATicTacToeAIController::FirstAIRandomMove(int Row, int Column)
{
	const ATicTacToePlayerState* TicTacToePlayerState = GetPlayerState<ATicTacToePlayerState>();
	GameState->TryToCaptureTile(TicTacToePlayerState->GetPlayerMarker(), Row, Column);
}

TPair<int, TPair<int, int>> ATicTacToeAIController::Minimax(TArray<FGrid1dArray>& Grid, int Depth, bool bIsMaximizing, int Alpha, int Beta,
                                                            int Row, int Column)
{
	int BestScore = bIsMaximizing ? INT_MIN: INT_MAX;
	TPair<int, int> BestMove = TPair<int, int>(-1, -1);

	TEnumAsByte<ETileState> Winner;
	if (Depth == 0 || GameState->CheckVictory(Row, Column, Winner))
	{
		if (bIsMaximizing)
		{
			return TPair<int, TPair<int, int>>(INT_MIN, BestMove);
		}
		return TPair<int, TPair<int, int>>(INT_MAX, BestMove);
	}
	
	if (!GameState->CheckDraw())
	{
		return TPair<int, TPair<int, int>>(0, BestMove);
	}	
	
	TArray<TPair<int, int>> LegalMoves = GetLegalMoves();

	if (bIsMaximizing)
	{
		for (const TPair<int, int> Pair : LegalMoves)
		{
			Grid[Pair.Key][Pair.Value] = GameState->SecondPlayerMarker;
			BestScore = FMath::Max(BestScore, Minimax(Grid, Depth - 1, false, Alpha, Beta, Row, Column).Key);
			Grid[Pair.Key][Pair.Value] = FREE;
			BestMove = Pair;
			if (BestScore >= Beta)
			{
				break;					
			}
			Alpha = FMath::Max(Alpha, BestScore);	
		}
		return TPair<int, TPair<int, int>>(BestScore, BestMove);
	}
	for (const TPair<int, int> Pair : LegalMoves)
	{
		Grid[Pair.Key][Pair.Value] = GameState->FirstPlayerMarker;
		BestScore = FMath::Min(BestScore, Minimax(Grid, Depth - 1, true, Alpha, Beta, Row, Column).Key);
		Grid[Pair.Key][Pair.Value] = FREE;
		BestMove = Pair;
		if (BestScore <= Alpha)
		{
			break;					
		}
		Beta = FMath::Min(Beta, BestScore);
	}
	return TPair<int, TPair<int, int>>(BestScore, BestMove);
}
